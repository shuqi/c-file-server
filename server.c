#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <dirent.h>
#include <limits.h>

#define PORT_NUMBER 8080
#define SERVER_ADDRESS "127.0.0.1"
#define TRUE 1
#define FALSE 0

void *receive_message(void *ptr){
	int *peer_socket;
	char *reply;

	peer_socket = (int *) ptr;

	int running = TRUE;
	while(running == TRUE){
		reply = (char*)calloc(BUFSIZ, sizeof(char));
		if(recv(*peer_socket, reply, BUFSIZ, 0) < 0){
			puts("recv failed");
			fprintf(stderr, "Error on listen --> %s", strerror(errno));
			exit(EXIT_FAILURE);
			running = FALSE;
		}
		else{
			//printf("Reply received: %s\n", reply);
		}
		if ((strcmp(reply, "quit") == 0) || (strcmp(reply, "") == 0)){
			puts("CONNECTION CLOSED\n");
			close(*peer_socket);
			running = FALSE;
			break;
		}
		else if (strlen(reply) > 3){
			//Parse command
			//Get first two letteres (command)
			char *command, *target;
			FILE *target_file;
			command = (char*)malloc(2*sizeof(char));
			target = (char*)malloc((strlen(reply) - 3)*sizeof(char));
			strncpy(command, reply, 2);
			strncpy(target, reply + 3, strlen(reply) - 3);
			//printf("Command: %s\nTarget: %s\n", command, target);

			//Open File
			//if
			//target_file = fopen(target, );

			if(strcmp(command, "cd") == 0){
				printf("Move to client directory: %s\n", target); //NO USE ON SERVER! keep on client side!
			}
			else if(strcmp(command, "sd") == 0){
				printf("Move to server directory: %s\n", target);
			}
			else if(strcmp(command, "up") == 0){
				//int socket_desc;
				char *message;
				char *upreply;
				//struct message_data *passed_data;
				upreply = (char*)malloc(BUFSIZ*sizeof(char));
				//passed_data = (struct message_data*)malloc(1*sizeof(struct message_data));
				//passed_data = (struct message_data *) ptr;
				//socket_desc = passed_data->socket;
				message = reply;

				//SEND ACKNOWLEDGE
				send(*peer_socket, "upack", BUFSIZ, 0);
				char *fr_name, *filename;
				char *reply_buffer;
				int shortened = FALSE;

				filename = target;
				//recv(socket_desc, fr_name, BUFSIZ, 0);
				for(int i = strlen(filename) - 1; i >= 0; i--){
					if(filename[i] == '/'){
						fr_name = (char*)calloc(strlen(filename) - i, sizeof(char));
						fr_name = strncpy(fr_name, filename + i + 1, strlen(filename) - i);	
						shortened = TRUE;
						break;
					}
				}

				if(shortened == FALSE){
					fr_name = filename;
				}

				//printf("PASSED FILENAME + %s\n", fr_name);

				reply_buffer = (char*)calloc(BUFSIZ, sizeof(char));
				FILE *fr = fopen(fr_name, "a");
				//printf("FILE ALLOCATED\n");

				//int fr_block_size = 0;
			    int loop_number = 0;

			    printf("File is: %s\n", fr_name);
				if(fr == NULL){
					printf("File %s cannot be opened.\n", fr_name);
				}
				else
				{
					//printf("FILE OPENED!\n");
				    int fr_block_size = 0;
				    while((fr_block_size = recv(*peer_socket, reply_buffer, 512, 0)) > 0)
				    {
				    	//printf("Writing...\n");
				        int write_size = fwrite(reply_buffer, sizeof(char), fr_block_size, fr);
				        if(write_size < fr_block_size)
				        {
				            printf("File write failed.\n");
				        }
				        if (fr_block_size == 0 || fr_block_size != 512) 
				        {
				            break;
				        }
				    }
				    if(fr_block_size < 0)
				    {
				        if (errno == EAGAIN)
				        {
				            printf("recv() timed out.\n");
				        }
				        else
				        {
				            fprintf(stderr, "recv() failed due to errno = %d\n", errno);
				        }
				    }
				    printf("Ok received from server!\n");
					/*RECIEVING FILE FROM SERVER END*/
				    fclose(fr);
				}
				//printf("Sent message: %s\n", message);
				//Succeed
			}
			else if(strcmp(command, "dl") == 0){
				printf("Download from server: %s\n", target);
				send(*peer_socket, "dlack", BUFSIZ, 0);

				/*SENDING FILE TO CLIENT (almost the same as sending file from client.c)*/
				char* fs_name; //directory of the file or the file that the client will get.
				char* file_buffer; // Send buffer

				fs_name = target;
				file_buffer = (char*)calloc(BUFSIZ, sizeof(char));
				printf("[Server] Sending %s to the Client...", fs_name);
				FILE* fs = fopen(fs_name, "r");
				if(fs == NULL)
				{
					fprintf(stderr, "ERROR: File %s not found on server. (errno = %d)\n", fs_name, errno);
					exit(1);
				}

				int fs_block_size; 
				while((fs_block_size = fread(file_buffer, sizeof(char), 512, fs))>0)
				{
					if(send(*peer_socket, file_buffer, fs_block_size, 0) < 0)
					{
						fprintf(stderr, "ERROR: Failed to send file %s.\n(errno = %d)\n", fs_name, errno);
						exit(1);
					}
				}
				printf("Ok sent to client!\n");
				fclose(fs);
				/*SENDING FILE TO CLIENT END*/
			}
			else if(strcmp(command, "dt") == 0){
				printf("Delete from server: %s\n", target);
			}
		}
		else{
			if(strcmp(reply, "ls") == 0){
				char *cwd, *current_message;
				cwd = (char*)calloc(PATH_MAX + 1, sizeof(char));
				current_message = (char*)calloc(PATH_MAX + 1, sizeof(char));

				//printf("Size of cwd: %lu\n", sizeof(cwd));
				if(getcwd(cwd, PATH_MAX + 1) != NULL){
					//Append to message
					// current_message = strcat("Current directory: ", cwd);
					// if(send(*peer_socket, current_message, strlen(current_message), 0) < 0){
					// 	fprintf(stderr, "Error sending file list --> %s\n", strerror(errno));
					// }
					printf("\nCurrent directory: %s\n", cwd);

				}
				else{
					fprintf(stderr, "Error on listing files --> %s\n", strerror(errno));
				}
				//List files on client
				DIR *dir;
				struct dirent *ent;
				printf("Files in directory:\n\n");
				if((dir = opendir(cwd)) != NULL){
					while((ent = readdir(dir)) != NULL){
						printf("%s\n", ent->d_name);						
						if(send(*peer_socket, ent->d_name, BUFSIZ, 0) < 0){
							fprintf(stderr, "Error sending file list --> %s\n", strerror(errno));
						}
					}
				closedir(dir);
				} else{
					//printf("Error while printing directory files");
					fprintf(stderr, "Error printing directory files --> %s\n", strerror(errno));
				}
				printf("\n");
				//Sends end of ls
				if (send(*peer_socket, "endls", BUFSIZ, 0) < 0){	//move sending to different part
					puts("Send failed");
					//return 1;
				} else{
					//printf("Data sent: %s\n", message);
				}
			}
			else{
				puts("COMMAND NOT FOUND\n");
			}
			//puts("COMMAND NOT FOUND\n");
		}
	}
}

int main(int argc, char **argv){
	int server_socket, peer_socket;
	int *peer_sockets;
	socklen_t sock_len;
	struct sockaddr_in server_addr, peer_addr;
	char *message;
	char *peer_reply;
	pthread_t thread1;
	pthread_t *threads;
	int iret1;
	int thread_index, thread_number;

	//Create server socket
	server_socket = socket(AF_INET, SOCK_STREAM, 0);

	if(server_socket == -1){
		fprintf(stderr, "Error creating socket --> %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	//Reset server_addr struct
	memset(&server_addr, 0, sizeof(server_addr));

	//Construct server_addr struct
	server_addr.sin_family = AF_INET;
	inet_pton(AF_INET, SERVER_ADDRESS, &(server_addr.sin_addr));
	server_addr.sin_port = htons(PORT_NUMBER);

	//Bind
	if ((bind(server_socket, (struct sockaddr *)&server_addr, sizeof(struct sockaddr))) == -1){
		fprintf(stderr, "Error on bind --> %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	//Listening to incoming connections
	if((listen(server_socket, 5)) == -1){
		fprintf(stderr, "Error on listen --> %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	threads = (pthread_t*)malloc(1*sizeof(pthread_t));
	peer_sockets = (int*)malloc(1*sizeof(int));
	thread_number = 0;
	//Accept incoming peers
	int running = TRUE;
	while(running == TRUE){
		peer_sockets = (int*)realloc(peer_sockets, (thread_number + 1)*sizeof(int));
		peer_sockets[thread_number] = accept(server_socket, (struct sockaddr *)&peer_addr, &sock_len);
		if (peer_sockets[thread_number] == -1){
			fprintf(stderr, "Error on listen --> %s", strerror(errno));
			exit(EXIT_FAILURE);
		}
		thread_number++;
		fprintf(stdout, "Accept peer --> %s\n", inet_ntoa(peer_addr.sin_addr));
	
		//Launch new receiving thread
		if(thread_number > 1){
			//pthread_t *temp;
			threads = (pthread_t*)realloc(threads, thread_number*sizeof(pthread_t));
			//threads = temp;
		}
		thread_index = thread_number - 1;
		iret1 = pthread_create(&threads[thread_index], NULL, receive_message, &peer_sockets[thread_index]);
		
		//pthread_join(threads[thread_index], NULL);
	}

	for(int i = 0; i < thread_number; i++){
		pthread_join(threads[i], NULL);
	}
	close(server_socket);
	return 0;
}

/*
References:
Socket Programming:
http://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c
http://www.binarytides.com/socket-programming-c-linux-tutorial/
http://stackoverflow.com/questions/11952898/c-send-and-receive-file
http://www.cems.uwe.ac.uk/~irjohnso/linsock/Book%20Notes/Appendices/Socket%20API/Communications/sendfile.html
http://man7.org/linux/man-pages/man2/sendfile.2.html
http://stackoverflow.com/questions/7780809/is-it-possible-to-print-out-only-a-certain-section-of-a-c-string-without-making
*/